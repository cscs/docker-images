# Python XML

Base python image with pre-built XML, git and SOAP components for speeding up test and builds.

## Docker Publish to Gitlab Template 

```
docker build -t cscs/python-xml python-xml
docker tag cscs/python-xml registry.gitlab.developers.cam.ac.uk/cscs/ivanti-service-manager/ivanti-api-python-client-library/python-xml:3.6-alpine
docker push registry.gitlab.developers.cam.ac.uk/cscs/ivanti-service-manager/ivanti-api-python-client-library/python-xml:3.6-alpine
```